//importando el paquete de colors
require('colors') //
const argv = require ('yargs').argv; //paquetes arriba


//importacion de archivos abajo
const { generarArchivos } = require('./helpers/multiplicar')

// console.log(process.argv);
// console.log(argv);


// console.log('base: yargs', argv.base);
//const base = 6;


//este proceso es el mismo para cualquier ves que querramos hacer esto
// const [, , arg3= 'base=7'] = process.argv;
// const [, base] = arg3.split('=')
// console.log(base);


generarArchivos(argv.base)
  .then(nombreArchivo => console.log(nombreArchivo.rainbow, 'creado'))
  .catch(err => console.log(err))
