//importando el paquete de colors
require('colors') //

//importacion de paquetes
const fs = require('fs');

const crearArchivo = async (base = 5)=> { //este valor quiere secir que si no coloco un valor, el valor sera 5
  try {

    let salida = '';

    console.log('=================='.green);
    console.log(` Tabla del ${base}  `.green);
    console.log('=================='.green);

    for(let i = 1; i<=10; i++){
      salida += `${base} x ${i} = ${base* i}\n`;
    }

    console.log(salida);

    fs.writeFileSync(`Tabla-${base}.txt`, salida);
    return `Tabla-${base}.txt`; //este return es lo que devuelve en el then
  }
  catch (error){
    throw error;
  }
};

//exportacion del archivo
module.exports = { //asi se exportan arquivos
  generarArchivos: crearArchivo //se le coloca una clave o nombre (generarArchivos)
};
//la importacion esta en el app.js
